import React from 'react'
import PropTypes from 'prop-types'
import {Link} from "react-router-dom"
//const persona = {"nombre": "valdemar", "apellido":"Ortiz","edad": 20}


const CourseCard = ({id,title,image,price,professor}) => (
	<article className="card">
  <div className="img-container s-ratio-16-9 s-radius-tr s-radius-tl">
    <Link to= {`/CourseCard/${id}`} >
      <img src={image} alt={title}/>
    </Link>
  </div>

  <div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
    	<h3 className="center">{title}</h3>
    <div className="s-mb-2 s-main-center">
      <div className="card__teacher s-cross-center">
        <span className="small">{professor}</span>
      </div>
    </div>

    <div className="s-main-center">
      <a className="button--ghost-alert button--tiny" href="google.com">{`$ ${price}`}</a>
    </div>
  </div>
</article>
)

CourseCard.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  price: PropTypes.number,
  professor: PropTypes.string
}

CourseCard.defaultProps = {
  title: "No se encontro titulo",
  image: "https://i.ytimg.com/vi/NmfKKZ-ZXTg/maxresdefault.jpg",
  price: "--",
  professor: ""
}


export default CourseCard
import React, {useState,useEffect} from "react"
import Axios from 'axios'

const Course = ({match}) => {
    // el useState devuelve el state(course), y un callback para modificar el state
    // iniciamos el estado vacio para hacer la peticion al backend
    const [course, setCourse] = useState({})
    const [comment, setComment] = useState("sin comentarios")
    // recibe un callback y dentro podemos ejecutar la funcionalidad, esta funcion va a ser ejecutado cada vex que se renderice se ejecuta useEffe
    /*hcamos la peticion con axios, el segundo param es para que se renderize una vez, en caso contrario se crea 
    un loop infinito
    olimpia https://i.pinimg.com/236x/fe/d1/5b/fed15b4f3f8d8325fd669e9e5879189d--soccer-logo-football-soccer.jpg
    cerro https://upload.wikimedia.org/wikipedia/commons/f/f3/Logo_del_Club_Cerro_Porte%C3%B1o_bySrCena.png
    guarani https://cdn.worldvectorlogo.com/logos/club-guarani.svg
      */
    useEffect(() => {
        Axios.get(`http://my-json-server.typicode.com/valdeortiz/json-db/courses/${match}`)
        .then(resp => setCourse(resp.data))
    }, [])

    const MyComment = (e) => {
        setComment(e.target.value)
    }

    return (
        <div className="ed-grid m-grid-3">
        {
            course ? (
            <div className="ed-grid">
                <div className="l-block">
                    <h1 className="m-cols-3"> {course.titulo} </h1>
                    <img className="m-cols-1" src={course.image} alt="Imagen del curso" />
                    <p className="m-cols-2">profesor: {course.profesor} </p>
                </div>
                <div className="ed-grid">
                    <h2>Escribe tu comentario</h2>
                    <input type="text" placeholder="Escribe ...." onChange={MyComment.bind(this)} />
                    <p>{comment}</p>
                </div>
                
            </div>
        ) : <h1>El curso no existe</h1>
        }
        </div>        
    )
}

export default Course
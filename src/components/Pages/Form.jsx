import React, {Component} from 'react'
//import { element } from 'prop-types'

class Form extends Component {

    constructor(props){
        super(props)

        this.state = {
            name: "",
            email: "",
            date: new Date()
        }
        // enlaza el metodo changeEmail al input de la funcion
        this.changeEmail = this.changeEmail.bind(this)
        this.changeName = this.changeName.bind(this)
        this.changeDate = this.changeDate.bind(this)
    }

    changeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }
    changeName(e){
        this.setState({
            name: e.target.value
        })
    }

    changeDate(){
        this.setState({
            date: new Date()
        })
    }

    // este metodo es para actualizar el estado, recibe un objeto
    // this.setState({})

    // lo que se renderiza en el cliente es lo que retorna este metodo.. Es el mas importante
    render(){
        return (
            <div className="ed-grid">
                <h1>Formulario {this.props.name}</h1>
                <h4>date actual: {Math.ceil(this.state.date/1000)}</h4>
                <form id="form-element">
                    <div className="ed-grid m-grid-2">
                        <div className="form__item">
                            <label>name Completo</label>
                            <input type="text"
                                onChange={this.changeName}
                            />
                        </div>
                        <div className="form__item">
                            <label>email </label>
                            <input type="email"
                                onChange={this.changeEmail}
                            />
                        </div>
                    </div>
                </form>
                <div>
                    <h2>{`Hola ${this.state.name}`}</h2>
                    <span>{`Tu email es: ${this.state.email}`}</span>
                </div>
            </div>
        )
    }
    // este metodo se ejecuta despues de actulizar y renderizar el DOM
    componentDidMount(){
        let element = document.getElementById("form-element")
        console.log(element)
        this.intervalodate = setInterval(() => {
            this.changeDate()
        }, 1000)

    }
    // este metodo se ejecuta al final de la actulizacion guarda el estado previo a la actulizacion
    componentDidUpdate(prevProps, prevState){
        console.log(prevProps)
        console.log("-------")
        console.log(prevState)
        console.log("-------")
    }
    //se ejecuta al demontar el componente
    componentWillUnmount(){
        clearInterval(this.intervalodate)
    }

}

//Ciclo de vida de montaje
// Constructor-render-se actuliza el DOM-componentDidMount
//ciclo de de vida de una actualizacion
//render - actuliza el DOM - componentDidUpdate
// ciclo de desmontaje
// componentDidUnmount

export default Form